import {UserRecipe} from './userRecipe';
import {RecipeIngredient} from './recipeingredient';

export interface Recipe {
  id: number;
  name: string;
  recipes: Set<UserRecipe>;
  recipeIngredients: Set<RecipeIngredient>;
  description: string;
  breakfast: boolean;
  lunch: boolean;
  dinner: boolean;
  snack: boolean;
  servings: number;
  picture: string;
  alt: string;
  prepCookTime: string;
}
