import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RecipeService} from '../recipe.service';
import {Recipe} from '../../recipe';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {merge, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

  constructor(private http: HttpClient,
              private recipeService: RecipeService,
              private router: Router) {
  }

  recipe: Recipe;
  model: any;
  recipeOptions: Recipe[] = [];
  recipeOptionValues: string[] = [];

  @ViewChild('instance', {static: true}) instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  ngOnInit(): void {
    this.http.get<Recipe[]>('/api/recipes')
      .subscribe(recipes => {
        this.recipeOptions = recipes;
        for (const recipe of this.recipeOptions) {
          this.recipeOptionValues.push(recipe.name);
        }
      });
  }

  searchKeyword = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.recipeOptionValues
        : this.recipeOptionValues.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  searchRecipe() {
    for (const recipe of this.recipeOptions) {
      if (recipe.name === this.model) {
        this.http.get<Recipe>('/api/recipe/' + recipe.id.toString())
          .subscribe(recipee => {
              this.recipe = recipe;
              this.recipeService.setRecipe(this.recipe);
              this.router.navigate(['/recipeDetail/' + recipe.id.toString()]);
            }
          );
      }
    }
  }
}
