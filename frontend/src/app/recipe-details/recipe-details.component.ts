import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Recipe} from '../../recipe';
import {ActivatedRoute} from '@angular/router';
import {RecipeIngredient} from '../../recipeingredient';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {UserRecipe} from '../../userRecipe';
import {RecipeService} from '../recipe.service';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {

  sessionUser: User | null = null;
  recipe: Recipe;
  favouriteUserRecipes: UserRecipe[] = [];
  favouriteRecipeIds: number[] = [];
  private id: number;
  private sub: any;
  private idString: string;
  recipeIngredients: RecipeIngredient[];

  constructor(private route: ActivatedRoute, private recipeService: RecipeService,
              private http: HttpClient,  private securityService: SecurityService) {
  }

  ngOnInit() {

    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
        if (u != null) {
          const idUser = this.sessionUser.id.toString();
          this.http.get<UserRecipe[]>('/api/favouriterecipes/' + idUser)
            .subscribe(favouriteRecipes => {
              this.favouriteUserRecipes = favouriteRecipes;
              for (const userrecipe of favouriteRecipes) {
                this.favouriteRecipeIds.push(userrecipe.recipe.id);
              }
            });
        }
      }
    );

    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id;
    });
    this.idString = this.id.toString();
    this.recipeService.currentRecipe.subscribe(recipe => this.recipe = recipe);

    this.http.get<Recipe>('/api/recipe/' + this.idString)
      .subscribe(recipe => {
        this.recipe = recipe;
      });

    this.http.get<RecipeIngredient[]>('/api/' + this.idString + '/recipeingredient')
      .subscribe(recipeIngredients => {
        this.recipeIngredients = recipeIngredients;
      });
  }

  markRecipeAsFavourite(recipe) {
    const idUser = this.sessionUser.id.toString();
    const idRecipe = recipe.id.toString();
    this.http.post<UserRecipe[]>('/api/markrecipefavourite/' + idUser + '/' + idRecipe, null)
      .subscribe(favourites => {
        this.favouriteUserRecipes = favourites;
        for (const favouriteRecipe of favourites) {
          this.favouriteRecipeIds.push(favouriteRecipe.recipe.id);
        }
      });
  }
}
