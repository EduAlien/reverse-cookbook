import {Injectable} from '@angular/core';
import {Recipe} from '../recipe';
import {BehaviorSubject, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  private recipe: Recipe;
  private recipeSource = new BehaviorSubject<Recipe>(this.recipe);
  currentRecipe = this.recipeSource.asObservable();

  constructor() {
  }

  setRecipe(recipe: Recipe) {
    this.recipeSource.next(recipe);
  }
}
