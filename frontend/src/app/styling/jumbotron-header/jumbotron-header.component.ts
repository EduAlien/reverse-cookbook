import {Component, OnInit} from '@angular/core';
import {User} from '../../user';
import {SecurityService} from '../../security.service';

@Component({
  selector: 'app-jumbotron-header',
  templateUrl: './jumbotron-header.component.html',
  styleUrls: ['./jumbotron-header.component.css']
})

export class JumbotronHeaderComponent implements OnInit {
  sessionUser: User | null = null;

  constructor(private securityService: SecurityService) {
  }

  ngOnInit(): void {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }
}
