import {Component, OnInit} from '@angular/core';
import {UserRecipe} from '../../userRecipe';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {Recipe} from '../../recipe';

@Component({
  selector: 'app-user-recipes',
  templateUrl: './user-recipes.component.html',
  styleUrls: ['./user-recipes.component.css']
})
export class UserRecipesComponent implements OnInit {

  favouriteRecipes: UserRecipe[];
  private sessionUser: User | null;
  recipes: Recipe[];
  recipe: Recipe;

  constructor(private http: HttpClient, private securityService: SecurityService) {
  }

  ngOnInit(): void {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    const idUser = this.sessionUser.id.toString();

    this.http.get<UserRecipe[]>('/api/favouriterecipes/' + idUser)
      .subscribe(favouriteRecipes => this.favouriteRecipes = favouriteRecipes);
  }

  unmarkRecipeAsFavourite(recipe) {
    this.favouriteRecipes.splice(recipe);
    const idUser = this.sessionUser.id.toString();
    const idRecipe = recipe.id.toString();

    this.http.post<UserRecipe[]>('/api/unmarkrecipefavourite/' + idUser + '/' + idRecipe, null)
      .subscribe(favourites => this.favouriteRecipes = favourites);
  }
}
