import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AllRecipesComponent} from './all-recipes/all-recipes.component';
import {RecipeDetailsComponent} from './recipe-details/recipe-details.component';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {UserRecipesComponent} from './user-recipes/user-recipes.component';


const routes: Routes = [
  {path: '', component: AllRecipesComponent},
  {path: 'recipes', component: AllRecipesComponent},
  {path: 'recipeDetail/:id', component: RecipeDetailsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'register', component: RegistrationComponent},
  {path: 'yourprofile', component: UserRecipesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
