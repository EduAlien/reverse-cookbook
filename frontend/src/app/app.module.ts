import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionUserComponent } from './session-user/session-user.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AllRecipesComponent } from './all-recipes/all-recipes.component';
import {RecipeDetailsComponent} from './recipe-details/recipe-details.component';
import {BsDropdownModule, ModalModule, TooltipModule} from 'ngx-bootstrap';
import {JumbotronHeaderComponent} from './styling/jumbotron-header/jumbotron-header.component';
import {TopNavbarComponent} from './styling/top-navbar/top-navbar.component';
import {FooterComponent} from './styling/footer/footer.component';
import { SidebarcolumsComponent } from './styling/sidebarcolums/sidebarcolums.component';
import { RouterModule, Routes } from '@angular/router';
import {NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import { RegistrationComponent } from './registration/registration.component';
import { UserRecipesComponent } from './user-recipes/user-recipes.component';
import { SearchbarComponent } from './searchbar/searchbar.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'AllRecipesComponent'},
  { path: 'login', component: LoginComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    SessionUserComponent,
    LoginComponent,
    AllRecipesComponent,
    RecipeDetailsComponent,
    JumbotronHeaderComponent,
    TopNavbarComponent,
    FooterComponent,
    SidebarcolumsComponent,
    RegistrationComponent,
    UserRecipesComponent,
    SearchbarComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    ReactiveFormsModule,
    NgbTypeaheadModule
  ],

  providers: [],

  bootstrap: [AppComponent]
})

export class AppModule { }
