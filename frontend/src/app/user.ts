import {UserRecipe} from '../userRecipe';

export interface User {
  id: number;
  username: string;
  password: string;
  admin: boolean;
  recipeList: Set<UserRecipe>;
}
