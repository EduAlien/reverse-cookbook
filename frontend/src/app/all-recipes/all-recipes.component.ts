import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Recipe} from '../../recipe';
import {Ingredient} from '../../ingredient';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {UserRecipe} from '../../userRecipe';
import {RecipeService} from '../recipe.service';

@Component({
  selector: 'app-all-recipes',
  templateUrl: './all-recipes.component.html',
  styleUrls: ['./all-recipes.component.css']
})

export class AllRecipesComponent implements OnInit {

  sessionUser: User | null = null;
  recipes: Recipe[];
  favouriteUserRecipes: UserRecipe[] = [];
  favouriteRecipeIds: number[] = [];
  ingredients: Ingredient[];
  chosenIngredients: Ingredient[] = [];

  constructor(private http: HttpClient, private securityService: SecurityService) {
  }

  ngOnInit(): void {
    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
        if (u != null) {
          const idUser = this.sessionUser.id.toString();
          this.http.get<UserRecipe[]>('/api/favouriterecipes/' + idUser)
            .subscribe(favouriteRecipes => {
              this.favouriteUserRecipes = favouriteRecipes;
              for (const userRecipe of favouriteRecipes) {
                this.favouriteRecipeIds.push(userRecipe.recipe.id);
              }
            });
        }
      }
    );

    this.http.get<Recipe[]>('/api/recipes')
      .subscribe(recipes => this.recipes = recipes);

    this.http.get<Ingredient[]>('/api/ingredients')
      .subscribe(ingredients => {
        this.ingredients = ingredients;
      });
  }

  search() {
    for (const ingredient of this.ingredients) {
      if (ingredient.isAvailable) {
        this.chosenIngredients.push(ingredient);
      }
    }
    this.http.post<Recipe[]>('/api/recipesearch', this.chosenIngredients)
      .subscribe(recipes => this.recipes = recipes);
  }

  showAllRecipes() {
    this.http.get<Recipe[]>('/api/recipes')
      .subscribe(recipes => this.recipes = recipes);
  }

  markRecipeAsFavourite(recipe) {
    const idUser = this.sessionUser.id.toString();
    const idRecipe = recipe.id.toString();
    this.http.post<UserRecipe[]>('/api/markrecipefavourite/' + idUser + '/' + idRecipe, null)
      .subscribe(favourites => {
        this.favouriteUserRecipes = favourites;
        for (const favouriteRecipe of favourites) {
          this.favouriteRecipeIds.push(favouriteRecipe.recipe.id);
        }
      });
  }

  removeCheckedIngredients() {
    for (const ingredient of this.chosenIngredients) {
      ingredient.isAvailable = false;
    }
    const checkboxes = document.getElementsByTagName('input');
    for (let i = 0; i < checkboxes.length; i++) {
      checkboxes.item(i).checked = false;
    }
    this.chosenIngredients.length = 0;
  }
}
