import {User} from './app/user';
import {Recipe} from './recipe';

export interface UserRecipe {
id: number;
user: User;
recipe: Recipe;
}
