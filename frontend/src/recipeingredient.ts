import {Recipe} from './recipe';
import {Ingredient} from './ingredient';

export interface RecipeIngredient {
id: number;
quantity: number;
weight: number;
ingredient: Ingredient;
recipe: Recipe;
}
