import {RecipeIngredient} from './recipeingredient';

export interface Ingredient {
  id: number;
  name: string;
  isAvailable: boolean;
  recipeIngredients: Set<RecipeIngredient>;
  category: string;
  allergens: string;
}
