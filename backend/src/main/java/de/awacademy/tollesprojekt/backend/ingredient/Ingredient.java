package de.awacademy.tollesprojekt.backend.ingredient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.awacademy.tollesprojekt.backend.manyToMany.RecipeIngredient;
import javax.persistence.*;
import java.util.Set;

/**
 * Ingredient is a database Entity
 */
@Entity
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @OneToMany(mappedBy = "ingredient")
    @JsonIgnore
    private Set<RecipeIngredient> recipeIngredients;

    private String category;

    private String allergens;

    public Ingredient() {
    }

    public Ingredient(String name, String allergens) {
        this.name = name;
        this.allergens = allergens;
    }

    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getCategory() {
        return category;
    }
    public String getAllergens() {
        return allergens;
    }
    public Set<RecipeIngredient> getRecipeIngredients() {
        return recipeIngredients;
    }

    public void setId(long id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setRecipeIngredients(Set<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public void setAllergens(String allergens) {
        this.allergens = allergens;
    }

}
