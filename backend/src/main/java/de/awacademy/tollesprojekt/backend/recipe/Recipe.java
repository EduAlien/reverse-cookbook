package de.awacademy.tollesprojekt.backend.recipe;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.awacademy.tollesprojekt.backend.manyToMany.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Recipe is a database Entity
 */
@Entity
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @OneToMany(mappedBy = "recipe")
    @JsonIgnore
    private Set<UserRecipe> recipes;

    @OneToMany(mappedBy = "ingredient")
    private Set<RecipeIngredient> recipeIngredients;

    @Column(length = 9999)
    private String description;

    private int servings;

    @Column(length = 9999)
    private String picture;
    private String alt;

    private String prepCookTime;

    //Mealtime Categories as boolean flags
    private boolean isBreakfast;
    private boolean isLunch;
    private boolean isDinner;
    private boolean isSnack;

    //Default and specified Constructor
    public Recipe() {
    }

    public Recipe(String name, String description, int servings, Set<RecipeIngredient> recipeIngredients,
                  String picture, String alt, String prepCookTime, boolean isBreakfast, boolean isLunch,
                  boolean isDinner, boolean isSnack) {
        this.name = name;
        this.description = description;
        this.servings = servings;
        this.recipeIngredients = recipeIngredients;
        this.picture = picture;
        this.alt = alt;
        this.prepCookTime = prepCookTime;
        this.isBreakfast = isBreakfast;
        this.isLunch = isLunch;
        this.isDinner = isDinner;
        this.isSnack = isSnack;
    }

    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public Set<UserRecipe> getRecipes() {
        return recipes;
    }
    public Set<RecipeIngredient> getRecipeIngredients() {
        return recipeIngredients;
    }
    public String getDescription() {
        return description;
    }
    public boolean isBreakfast() {
        return isBreakfast;
    }
    public boolean isLunch() {
        return isLunch;
    }
    public boolean isDinner() {
        return isDinner;
    }
    public boolean isSnack() {
        return isSnack;
    }
    public int getServings() {
        return servings;
    }
    public String getPicture() {
        return picture;
    }
    public String getAlt() {
        return alt;
    }
    public String getPrepCookTime() {
        return prepCookTime;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setRecipes(Set<UserRecipe> recipes) {
        this.recipes = recipes;
    }
    public void setRecipeIngredients(Set<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setBreakfast(boolean breakfast) {
        isBreakfast = breakfast;
    }
    public void setLunch(boolean lunch) {
        isLunch = lunch;
    }
    public void setDinner(boolean dinner) {
        isDinner = dinner;
    }
    public void setSnack(boolean snack) {
        isSnack = snack;
    }
    public void setServings(int servings) {
        this.servings = servings;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }
    public void setPrepCookTime(String prepCookTime) {
        this.prepCookTime = prepCookTime;
    }
    public void setId(long id) {
        this.id = id;
    }
}
