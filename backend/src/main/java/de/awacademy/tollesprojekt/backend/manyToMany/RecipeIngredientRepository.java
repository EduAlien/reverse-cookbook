package de.awacademy.tollesprojekt.backend.manyToMany;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Set;

/**
 * RecipeIngredientRepository is the Repository of the RecipeIngredient table
 */
@Repository
public interface RecipeIngredientRepository extends CrudRepository<RecipeIngredient, Long> {

    Set<RecipeIngredient> findAll();

    Set <RecipeIngredient> findByIngredient_Id(long id);

    Set<RecipeIngredient> findAllByRecipeId(long id);
}
