package de.awacademy.tollesprojekt.backend.manyToMany;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * UserRecipesRepository is the Repository of the UserRecipe table
 */
@Repository
public interface UserRecipeRepository extends CrudRepository<UserRecipe, Long> {

    Set<UserRecipe> findAll();

    Set<UserRecipe> findAllByUserId(long id);

    UserRecipe findByUserIdAndRecipeId(long userId, long recipeId);
}
