package de.awacademy.tollesprojekt.backend.manyToMany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * UserRecipeController is a RestController for the UserRecipe table
 */
@RestController
public class UserRecipeController {

    private UserRecipeService userRecipeService;

    @Autowired
    public UserRecipeController(UserRecipeService userRecipeService) {
        this.userRecipeService = userRecipeService;
    }

    /**
     * The PostMapping method is used for marking a recipe as a favourite recipe of a specific user
     *
     * @param userId is the ID of the user
     * @param recipeId is the ID of the recipe
     * @return the return value is a Set of UserRecipes
     */
    @PostMapping("/api/markrecipefavourite/{userId}/{recipeId}")
    public Set<UserRecipe> markRecipeAsFavourite(@PathVariable("userId") long userId,
                                                 @PathVariable ("recipeId") long recipeId) {
        return userRecipeService.markRecipeAsFavourite(userId, recipeId);
    }

    /**
     * The PostMapping method is used for unmarking a recipe as a favourite recipe of a specific user
     *
     * @param userId is the ID of the user
     * @param recipeId is the ID of the recipe
     * @return the return value is a Set of UserRecipes
     */
    @PostMapping("/api/unmarkrecipefavourite/{userId}/{recipeId}")
    public Set<UserRecipe> unmarkRecipeAsFavourite(@PathVariable("userId") long userId, @PathVariable("recipeId") long recipeId) {
        return userRecipeService.unmarkRecipeAsFavourite(userId, recipeId);
    }

    /**
     * The GetMapping method is used for showing all favourite recipes of a specific user
     *
     * @param userId is the ID of the user
     * @return the return value is a Set of UserRecipes
     */
    @GetMapping("/api/favouriterecipes/{userId}")
    public Set<UserRecipe> showFavouriteRecipesById(@PathVariable ("userId") long userId) {
        return userRecipeService.getFavouriteRecipesByUserId(userId);
    }
}
