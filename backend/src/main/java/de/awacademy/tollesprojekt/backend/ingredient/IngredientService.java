package de.awacademy.tollesprojekt.backend.ingredient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * IngredientService is a service of the Ingredient table
 */
@Service
public class IngredientService {

    private IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * The method is used for getting all Ingredients from the database
     *
     * @return the return value is the list of Ingredients
     */
    public List<Ingredient> getAllIngredients() {
        return ingredientRepository.findAll();
    }

    /**
     * The method is used for getting all Ingredients in a specific category from the database
     *
     * @param category is the category of an Ingredient
     * @return the return value is a list of Ingredients
     */
    public List<Ingredient> getIngredientByCategory(String category) {
        return ingredientRepository.findAllByCategory(category);
    }

    /**
     * The method is used for adding an Ingredient to the database
     *
     * @param ingredient is the Ingredient object
     * @param category is the category of the passed Ingredient
     */
    public void addIngredient(Ingredient ingredient, String category) {
        ingredient.setCategory(category);
        ingredientRepository.save(ingredient);
    }

    /**
     * The method is used for getting an Ingredient from the database by its ID
     *
     * @param id is the ID of the Ingredient
     * @return the return value is the Ingredient
     */
    public Ingredient getIngredientById(long id) {
        return ingredientRepository.findById(id);
    }

    /**
     * The method is used for getting an Ingredient by its name
     *
     * @param name is the name of the Ingredient
     * @return the return value is the Ingredient
     */
    public Ingredient getIngredientByName(String name){
        return ingredientRepository.findByName(name);
    }
}
