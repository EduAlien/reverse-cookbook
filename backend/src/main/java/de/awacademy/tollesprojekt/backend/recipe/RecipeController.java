package de.awacademy.tollesprojekt.backend.recipe;

import de.awacademy.tollesprojekt.backend.ingredient.*;
import de.awacademy.tollesprojekt.backend.manyToMany.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * RecipeController is a RestController for the Recipe table
 */
@RestController
public class RecipeController {

    private RecipeService recipeService;
    private RecipeIngredientService recipeIngredientService;

    @Autowired
    public RecipeController(RecipeService recipeService, RecipeIngredientService recipeIngredientService) {
        this.recipeService = recipeService;
        this.recipeIngredientService = recipeIngredientService;
    }

    /**
     * The GetMapping method is used for showing all Recipes
     *
     * @return the return value is a list of Recipes
     */
    @GetMapping("/api/recipes")
    public List<Recipe> showAllRecipes() {
        return recipeService.getAllRecipes();
    }

    /**
     * The GetMapping method is used for showing the Recipe details of a specific Recipe
     *
     * @param id is the ID of a Recipe
     * @return the return value is a Recipe
     */
    @GetMapping("/api/recipe/{recipeId}")
    public Recipe showRecipeDetails(@PathVariable("recipeId") long id) {
        return recipeService.getRecipeById(id);
    }

    /**
     * The PostMapping method is used for showing Recipes from the chosen Ingredients
     *
     * @param chosenIngredients is a list of Ingredients
     * @return the return value is a list of Recipes
     */
    @PostMapping("/api/recipesearch")
    public List<Recipe> showRecipesFromChosenIngredients(@RequestBody List<Ingredient> chosenIngredients) {
        List<Recipe> foundRecipes = new ArrayList<>();
        for (Recipe recipe : recipeService.getAllRecipes()) {
            for (Ingredient ingredient : chosenIngredients) {
                Set<RecipeIngredient> recipeIngredients = recipeIngredientService.getRecipeIngredientsByIngredientId(ingredient.getId());
                for (RecipeIngredient ri : recipeIngredients) {
                    if (recipeIngredientService.getRecipeIngredientsByRecipeId(recipe.getId()).contains(ri) && !foundRecipes.contains(recipe)) {
                        foundRecipes.add(recipe);
                    }
                }
            }
        }
        return foundRecipes;
    }
}
