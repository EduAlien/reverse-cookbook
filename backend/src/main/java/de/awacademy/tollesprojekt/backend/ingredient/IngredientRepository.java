package de.awacademy.tollesprojekt.backend.ingredient;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * IngredientRepository is the Repository of the Ingredient table
 */
@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {

    List<Ingredient> findAll();

    List<Ingredient> findAllByCategory(String category);

    Ingredient findById(long id);

    Ingredient findByName(String name);
}
