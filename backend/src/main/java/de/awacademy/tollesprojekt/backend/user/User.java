package de.awacademy.tollesprojekt.backend.user;

import de.awacademy.tollesprojekt.backend.manyToMany.UserRecipe;

import javax.persistence.*;
import java.util.Set;

/**
 * User is a database Entity
 */
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String username;
    private String password;
    private boolean admin;

    @OneToMany(mappedBy = "user")
    private Set<UserRecipe> recipeList;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.admin = false;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public Set<UserRecipe> getRecipeList() {
        return recipeList;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public void setRecipeList(Set<UserRecipe> recipeList) {
        this.recipeList = recipeList;
    }
}
