package de.awacademy.tollesprojekt.backend.ingredient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * IngredientController is the RestController for the Ingredient table
 */
@RestController
public class IngredientController {

    private IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService){
        this.ingredientService = ingredientService;
    }

    /**
     * The GetMapping method is used for displaying all Ingredients in the database
     *
     * @return the return value is the list of Ingredients
     */
    @GetMapping("/api/ingredients")
    public List<Ingredient> showAllIngredients(){
        return ingredientService.getAllIngredients();
    }

    /**
     * The GetMapping method is used for displaying all Ingredients according to a specific category
     *
     * @param category is the Ingredient category (e.g. Baking and Grains, Beverages, Dairy...)
     * @return the return value is the list of Ingredients
     */
    @GetMapping("/api/{category}/ingredient")
    public List<Ingredient> showIngredientsAccordingToCategory(@PathVariable String category){
        return ingredientService.getIngredientByCategory(category);
    }

    /**
     * The GetMapping method displays an Ingredient according to its ID
     *
     * @param id is the ID of the Ingredient
     * @return the return value is the Ingredient
     */
    @GetMapping("/api/ingredient/{id}")
    public Ingredient showIngredient(@PathVariable long id){
        return ingredientService.getIngredientById(id);
    }
}
