package de.awacademy.tollesprojekt.backend.manyToMany;

import de.awacademy.tollesprojekt.backend.recipe.*;
import de.awacademy.tollesprojekt.backend.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * UserRecipesService is a service of the UserRecipes table
 */
@Service
public class UserRecipeService {

    private UserRecipeRepository userRecipeRepository;
    private UserService userService;
    private RecipeService recipeService;

    @Autowired
    public UserRecipeService(UserRecipeRepository userRecipeRepository,
                             UserService userService, RecipeService recipeService){
        this.userRecipeRepository = userRecipeRepository;
        this.userService = userService;
        this.recipeService = recipeService;
    }

    /**
     * The method is used for getting all favourite recipes of a specific user from the database
     *
     * @param id is the ID of the user
     * @return the return value is a Set of UserRecipes
     */
    public Set<UserRecipe> getFavouriteRecipesByUserId(long id) {
        return userRecipeRepository.findAllByUserId(id);
    }

    /**
     * The method is used for marking a recipe as a favourite recipe of a specific user in the database
     *
     * @param userId is the ID of the user
     * @param recipeId is the ID of the recipe
     * @return the return value is a Set of UserRecipes
     */
    public Set<UserRecipe> markRecipeAsFavourite(long userId, long recipeId) {
        if (userRecipeRepository.findByUserIdAndRecipeId(userId, recipeId) == null){
            User user = userService.getUserById(userId);
            Recipe recipe = recipeService.getRecipeById(recipeId);
            userRecipeRepository.save(new UserRecipe(user, recipe));
        }
        return getFavouriteRecipesByUserId(userId);
    }

    /**
     * The method is used for unmarking a recipe as a favourite recipe of a specific user in the database
     *
     * @param userId is the ID of the user
     * @param recipeId is the ID of the recipe
     * @return the return value is a Set of UserRecipes
     */
    public Set<UserRecipe> unmarkRecipeAsFavourite(long userId, long recipeId) {
        UserRecipe userRecipe = userRecipeRepository.findByUserIdAndRecipeId(userId, recipeId);
        if (userRecipe != null){
            userRecipeRepository.delete(userRecipe);
        }
        return getFavouriteRecipesByUserId(userId);
    }
}
