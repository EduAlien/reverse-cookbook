package de.awacademy.tollesprojekt.backend.manyToMany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * RecipeIngredientController is a RestController for the RecipeIngredient table
 */
@RestController
public class RecipeIngredientController {

    private RecipeIngredientService recipeIngredientService;

    @Autowired
    public RecipeIngredientController(RecipeIngredientService recipeIngredientService){
        this.recipeIngredientService = recipeIngredientService;
    }

    /**
     * The GetMapping method is used for getting the RecipeIngredient according to its ID
     *
     * @param id is the ID of the RecipeIngredient
     * @return the return value is a Set of RecipeIngredients
     */
    @GetMapping("/api/{recipeId}/recipeingredient")
    public Set<RecipeIngredient> getRecipeIngredient(@PathVariable ("recipeId") long id){
        return recipeIngredientService.getRecipeIngredientsByRecipeId(id);
    }
}
