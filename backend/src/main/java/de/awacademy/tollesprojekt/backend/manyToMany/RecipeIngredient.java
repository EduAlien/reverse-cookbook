package de.awacademy.tollesprojekt.backend.manyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.awacademy.tollesprojekt.backend.ingredient.Ingredient;
import de.awacademy.tollesprojekt.backend.recipe.Recipe;
import javax.persistence.*;

/**
 * RecipeIngredient is a database Entity
 */
@Entity
public class RecipeIngredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int quantity;
    private double weight;

    @ManyToOne
    @JoinColumn(name = "ingredient_id")
    private Ingredient ingredient;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;

    public RecipeIngredient() {
    }

    public RecipeIngredient(Recipe recipe, Ingredient ingredient, int quantity, double weight){
        this.recipe = recipe;
        this.ingredient = ingredient;
        this.quantity = quantity;
        this.weight = weight;
    }

    public long getId() {
        return id;
    }
    public int getQuantity() {
        return quantity;
    }
    public double getWeight() {
        return weight;
    }
    public Ingredient getIngredient() {
        return ingredient;
    }
    public Recipe getRecipe() {
        return recipe;
    }

    public void setId(long id) {
        this.id = id;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }
    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
}
