package de.awacademy.tollesprojekt.backend.recipe;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * RecipeRepository is the Repository of the Recipe table
 */
@Repository
public interface RecipeRepository extends CrudRepository<Recipe, Long> {

    List<Recipe> findAll();

    Recipe findRecipeById(long id);

    Recipe findRecipeByName(String name);

}
