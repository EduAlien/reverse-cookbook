package de.awacademy.tollesprojekt.backend.manyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.awacademy.tollesprojekt.backend.recipe.Recipe;
import de.awacademy.tollesprojekt.backend.user.User;

import javax.persistence.*;

/**
 * UserRecipe is a database Entity
 */
@Entity
public class UserRecipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;

    public UserRecipe() {
    }

    public UserRecipe(User user, Recipe recipe) {
        this.user = user;
        this.recipe = recipe;
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
}
