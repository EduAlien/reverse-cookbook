package de.awacademy.tollesprojekt.backend;

import de.awacademy.tollesprojekt.backend.ingredient.*;
import de.awacademy.tollesprojekt.backend.manyToMany.*;
import de.awacademy.tollesprojekt.backend.recipe.*;
import de.awacademy.tollesprojekt.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;

/**
 * DummyDataService is a Service of the dummy data
 */
@Service
public class DummyDataService {

    private RecipeService recipeService;
    private IngredientService ingredientService;
    private RecipeIngredientService recipeIngredientService;
    private UserService userService;

    private List<Ingredient> bakingAndGrains = new ArrayList<>();
    private List<Ingredient> beverages = new ArrayList<>();
    private List<Ingredient> condiments = new ArrayList<>();
    private List<Ingredient> dairy = new ArrayList<>();
    private List<Ingredient> fruits = new ArrayList<>();
    private List<Ingredient> herbsAndSpices = new ArrayList<>();
    private List<Ingredient> legumes = new ArrayList<>();
    private List<Ingredient> meats = new ArrayList<>();
    private List<Ingredient> mushrooms = new ArrayList<>();
    private List<Ingredient> oils = new ArrayList<>();
    private List<Ingredient> seafood = new ArrayList<>();
    private List<Ingredient> vegetables = new ArrayList<>();

    private Set<RecipeIngredient> recipeIngredients = new HashSet<>();

    @Autowired
    public DummyDataService(RecipeService recipeService, IngredientService ingredientService,
                            RecipeIngredientService recipeIngredientService, UserService userService) {
        this.recipeService = recipeService;
        this.ingredientService = ingredientService;
        this.recipeIngredientService = recipeIngredientService;
        this.userService = userService;
        getDummyData();
    }

    /**
     * The method adds ingredients to the database within a category
     *
     * @param ingredientList is the name of the IngredientList
     * @param category       is the category of the IngredientList
     */
    public void addIngredientsToDatabase(List<Ingredient> ingredientList, String category) {
        for(Ingredient ingredient : ingredientList) {
            ingredientService.addIngredient(ingredient, category);
        }
    }

    /**
     * The method adds ingredients to a recipe
     *
     * @param recipeName is the name of the Recipe
     * @param ingrName   is the name of the Ingredient
     * @param quant      is the quantity of the Ingredient
     * @param weight     is the weight of the Ingredient
     */
    public RecipeIngredient addIngrToRec(String recipeName, String ingrName, int quant, int weight) {
        return new RecipeIngredient(recipeService.getRecipeByName(recipeName), ingredientService.getIngredientByName(ingrName), quant, weight);
    }

    /**
     * The method adds the the recipe ingredients to the database
     *
     * @param recipeIngredients is a list of RecipeIngredients
     */
    public void addRecIngrToDatabase(List<RecipeIngredient> recipeIngredients) {
        for(RecipeIngredient ingredient : recipeIngredients) {
            recipeIngredientService.addRecipeIngredient(ingredient);
        }
    }

    /**
     * The method generates dummy data
     */
    public void getDummyData() {
        // TestUser is registered, if they doesn't yet exist
        if (!userService.usernameExists("test")) {
            userService.register("test", "test1");
        }

        // Admin is registered, if they doesn't yet exist
        if (!userService.usernameExists("admin1")) {
            userService.registerAdmin("admin1", "admin");
        }

        recipeIngredients.add(new RecipeIngredient());

        bakingAndGrains.add(new Ingredient("Basmati Rice", ""));
        bakingAndGrains.add(new Ingredient("Spaghetti", ""));
        bakingAndGrains.add(new Ingredient("Wheat Flour", "gluten"));
        bakingAndGrains.add(new Ingredient("Breadcrumbs", "gluten"));
        bakingAndGrains.add(new Ingredient("Baguette", "gluten"));

        beverages.add(new Ingredient("Beer", "gluten"));
        beverages.add(new Ingredient("Wine", ""));

        condiments.add(new Ingredient("Ketchup", ""));
        condiments.add(new Ingredient("Mustard", ""));
        condiments.add(new Ingredient("Basil Pesto", ""));

        dairy.add(new Ingredient("Butter", "lactose"));
        dairy.add(new Ingredient("Cheese", "lactose"));
        dairy.add(new Ingredient("Eggs", ""));
        dairy.add(new Ingredient("Milk", "lactose"));
        dairy.add(new Ingredient("Whipped Cream", "lactose"));
        dairy.add(new Ingredient("Greek Yoghurt", "lactose"));
        dairy.add(new Ingredient("Mozzarella", "lactose"));

        fruits.add(new Ingredient("Apple", ""));
        fruits.add(new Ingredient("Banana", ""));
        fruits.add(new Ingredient("Lemon", ""));
        fruits.add(new Ingredient("Lime", ""));
        fruits.add(new Ingredient("Grapes", ""));
        fruits.add(new Ingredient("Pear", ""));
        fruits.add(new Ingredient("Mango", ""));
        fruits.add(new Ingredient("Strawberry", ""));

        herbsAndSpices.add(new Ingredient("Black Pepper", ""));
        herbsAndSpices.add(new Ingredient("Salt", ""));
        herbsAndSpices.add(new Ingredient("Sugar", ""));
        herbsAndSpices.add(new Ingredient("Vanilla Extract", ""));
        herbsAndSpices.add(new Ingredient("Chicken Broth", ""));
        herbsAndSpices.add(new Ingredient("Parsley", ""));

        legumes.add(new Ingredient("Chickpeas", ""));
        legumes.add(new Ingredient("Couscous", ""));
        legumes.add(new Ingredient("Kidney Beans", ""));

        meats.add(new Ingredient("Chicken Breast", ""));
        meats.add(new Ingredient("Minced Pork", ""));

        mushrooms.add(new Ingredient("White Bottom Mushroom", "")); //Champignon
        mushrooms.add(new Ingredient("Shiitake Mushroom", ""));
        mushrooms.add(new Ingredient("Chanterelle", "")); //Pfifferlinge

        oils.add(new Ingredient("Olive Oil", ""));
        oils.add(new Ingredient("Pumpkinseed Oil", ""));
        oils.add(new Ingredient("Vegetable Oil", ""));

        seafood.add(new Ingredient("Salmon", "fish"));
        seafood.add(new Ingredient("Shrimps", "shellfish"));
        seafood.add(new Ingredient("Smoked Salmon", "fish"));

        vegetables.add(new Ingredient("Bellpepper", "capseicin"));
        vegetables.add(new Ingredient("Eggplant", ""));
        vegetables.add(new Ingredient("Garlic", ""));
        vegetables.add(new Ingredient("Onion", ""));
        vegetables.add(new Ingredient("Potato", ""));
        vegetables.add(new Ingredient("Pumpkin", ""));
        vegetables.add(new Ingredient("Spring Onion", ""));
        vegetables.add(new Ingredient("Tomato", ""));
        vegetables.add(new Ingredient("Avocado", ""));

        if (recipeService.getRecipeRepository().count() == 0) {

            Recipe recipe1 = new Recipe("Omelette a la Uwe",
                    "1. Beat eggs, water, salt and pepper in small bowl until blended.\n" +
                            "2. Heat butter in 6 to 8-inch nonstick omelet pan or skillet over medium-high heat until hot. " +
                            "Tilt pan to coat bottom. Pour egg mixture into pan. Mixture should set immediately at edges.\n " +
                            "3. Gently push cooked portions from edges toward the center with inverted turner so that " +
                            "uncooked eggs can reach the hot pan surface. Continue cooking, tilting pan and moving cooked portions as needed.\n" +
                            "4. When top surface of eggs is thickened and no visible liquid egg remains, place filling " +
                            "on one side of the omelet. FOLD omelet in half with turner. With a quick flip of the wrist, " +
                            "turn pan and invert or slide omelet onto plate.", 1, recipeIngredients,
                    "https://cdn.pixabay.com/photo/2015/05/20/16/11/kitchen-775746_960_720.jpg",
                    "Photo by unknown on Pixabay", "10", true, false, false, false);
            Recipe recipe2 = new Recipe("Mona's Creamy Baked Pears",
                    "1. Preheat oven to 190 degrees C. Grease a 9-inch baking dish " +
                            "with 1 tablespoon butter. Sprinkle 1 tablespoon sugar into buttered dish.\n" +
                            "2. Rub remaining 1 tablespoon butter over pear halves; arrange pears cut sides down " +
                            "in prepared baking dish and sprinkle with remaining 1 tablespoon sugar.\n" +
                            "3. Bake in preheated oven for 10 minutes. Pour cream over pears and continue baking until tender, " +
                            "about 20 minutes more.", 1, recipeIngredients,
                    "https://images.unsplash.com/photo-1573098174852-18cec8b9cdc7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
                    "Photo by @khloephoto on Unsplash", "40", false, true, true, false);
            Recipe recipe3 = new Recipe("The Hummus Of Jonas",
                    "1. In a blender, chop garlic. Pour garbanzo beans into the blender, reserving about 1 tablespoon for garnish. " +
                            "Add reserved liquid, lemon juice, tahini, and salt to the blender. Blend until creamy and well mixed.\n" +
                            "2. Transfer the mixture to a medium serving bowl. Sprinkle with pepper and pour olive oil over the top. " +
                            "Garnish with reserved garbanzo beans.\n" +
                            "Serving Recommendation: Cut up some bread or any vegetables that can be eaten raw " +
                            "into sticks to dip into the hummus.", 1, recipeIngredients,
                    "https://images.unsplash.com/photo-1575201437021-d64f00562604?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80",
                    "Photo by @alanaharris on Unsplash", "15", false, true, false, true);
            Recipe recipe4 = new Recipe("Fab'licious Spicy Lime Grilled Shrimp",
                    "1. Mix together the Cajun seasoning, lime juice, and vegetable oil in a resealable plastic bag. " +
                            "Add the shrimp, coat with the marinade, squeeze out excess air, and seal the bag. " +
                            "Marinate in the refrigerator for 20 minutes.\n" +
                            "2. Preheat an outdoor grill for medium heat, and lightly oil the grate. " +
                            "Remove the shrimp from the marinade, and shake off excess. Discard the remaining marinade.\n" +
                            "3. Cook the shrimp on the preheated grill until they are bright pink on the outside and " +
                            "the meat is no longer transparent in the center, about 2 minutes per side.\n" +
                            "Serving Recommendation: Enjoy with some flatbread and pesto for dipping.", 1, recipeIngredients,
                    "https://images.unsplash.com/photo-1569172131007-4954763443d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1952&q=80",
                    "Photo by @miracleday on Unsplash ", "30", false, true, true, false);
            Recipe recipe5 = new Recipe("Jill Grill Baked Dijon Salmon",
                    "1. Preheat oven to 200 degrees Celsius. Line a shallow baking pan with aluminum foil.\n" +
                            "2. Place salmon skin-side down on foil. Spread a thin layer of mustard on the top of each fillet, " +
                            "and season with salt and pepper. Top with bread crumbs, then drizzle with melted butter.\n" +
                            "3. Bake in a preheated oven for 15 minutes, or until salmon flakes easily with a fork.\n" +
                            "Serving Recommendation: Serve with some fresh greens or an easy salad.", 4, recipeIngredients,
                    "https://cdn.pixabay.com/photo/2014/11/05/15/57/salmon-518032_960_720.jpg",
                    "Photo by unknown on Pixabay", "25", false, true, true, false);
            Recipe recipe6 = new Recipe("Tasty Pumpkin Soup by Umut",
                    "1. Combine pumpkin and 1/3 of the chicken broth in a deep pan over medium-low heat. " +
                            "Simmer, covered, until pumpkin is soft, about 15 minutes.\n" +
                            "2. Remove from heat and puree with an immersion blender. Add remaining chicken broth, sugar, salt, " +
                            "and pepper. Cook until flavors combine, about 5 minutes.\n" +
                            "3. Ladle into bowls and serve with a dollop of Greek yogurt.", 1, recipeIngredients,
                    "https://images.unsplash.com/photo-1541095441899-5d96a6da10b8?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
                    "Photo by @moniqa on Unsplash", "35", false, true, true, false);
            Recipe recipe7 = new Recipe("Maja's Dreamy Chickpea Curry With Rice",
                    "1. Cook the basmati rice according to the package instructions. 2. Heat the oil in a medium skillet " +
                            "over medium-low heat. Add the onions, season with salt and pepper and cook until the onions are " +
                            "dark brown and caramelized, about 10 minutes. 3. Stir in the curry powder and garlic and cook for 30 seconds. " +
                            "Pour in the vegetable stock and stir to scrape up all the brown bits in the pan. " +
                            "4. Add the chickpeas, coconut milk, honey and a squirt of sriracha. Bring to a boil, reduce the heat and simmer for 10 minutes. " +
                            "Serving Recommendation: Warm some naan in the microwave and serve the curry over the rice with it. Garnish with the coriander.", 4, recipeIngredients,
                    "https://images.unsplash.com/photo-1516714435131-44d6b64dc6a2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80",
                    "Photo by @marius_dragne on Unsplash", "35", false, true, true, false);
            Recipe recipe8 = new Recipe("Crispy Tim Bites Bruschetta",
                    "1. Preheat the oven to 200 degree C. Spray a small baking sheet with cooking spray. " +
                            "Toss the eggplant in a small bowl with the garlic and 1 Tablespoon olive oil. " +
                            "Spread out on the prepared baking sheet, sprinkle lightly with seasoning (or salt) and pepper " +
                            "and bake for about 15 minutes or until tender.\n 2. Meanwhile, put the tomatoes (with juices!) " +
                            "in the same bowl. Add the olive oil and vinegar and toss gently. When the eggplant is done, " +
                            "add it to the tomatoes and toss. Let sit for at least 5 minutes, tossing a couple times, " +
                            "so the eggplant can soak up some of the juices.\n " +
                            "3. On a cutting board, set several basil leaves on top of each other and roll them up into a log shape. " +
                            "Slice horizontally to create little ribbons. Gently fold the basil ribbons into the tomato bruschetta " +
                            "mixture.\n 4. Toast the bread and spread with goat cheese. Top with bruschetta and enjoy!",
                    6, recipeIngredients, "https://images.unsplash.com/photo-1505575967455-40e256f73376?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
                    "Photo by @nobiteuntilphoto on Unsplash", "25", false, true, true, false);
            Recipe recipe9 = new Recipe("Alex Loves Tasty Chicken Bake",
                    "1. Preheat the oven to 350° F. Season chicken breasts liberally with salt and pepper." +
                            "\n 2. Arrange chicken breasts (seared or raw) onto a baking dish or rimmed baking sheet." +
                            "\n 3. Use a spoon to cover chicken with pesto." +
                            "\n 4.Top each chicken breast with 1 slice of mozzarella cheese and 2 sliced tomatoes." +
                            "\n 5. Bake for 20- 30 minutes. Broil the chicken the final 2 minutes so that the cheese is bubbly and golden. Serve immediately.",
                    1, recipeIngredients, "https://images.unsplash.com/photo-1532550907401-a500c9a57435?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80",
                    "Photo by @tempestdesigner on Unsplash", "35", false, true, true, false);

            recipeService.addRecipe(recipe1);
            recipeService.addRecipe(recipe2);
            recipeService.addRecipe(recipe3);
            recipeService.addRecipe(recipe4);
            recipeService.addRecipe(recipe5);
            recipeService.addRecipe(recipe6);
            recipeService.addRecipe(recipe7);
            recipeService.addRecipe(recipe8);
            recipeService.addRecipe(recipe9);

            addIngredientsToDatabase(bakingAndGrains, "Baking and Grains");
            addIngredientsToDatabase(beverages, "Beverages");
            addIngredientsToDatabase(condiments, "Condiments");
            addIngredientsToDatabase(dairy, "Dairy");
            addIngredientsToDatabase(fruits, "Fruits");
            addIngredientsToDatabase(herbsAndSpices, "Herbs and spices");
            addIngredientsToDatabase(legumes, "Legumes");
            addIngredientsToDatabase(meats, "Meats");
            addIngredientsToDatabase(mushrooms, "Mushrooms");
            addIngredientsToDatabase(oils, "Oils");
            addIngredientsToDatabase(seafood, "Seafood");
            addIngredientsToDatabase(vegetables, "Vegetables");

            List<RecipeIngredient> ingredientsOfRecipe1 = new ArrayList<>();
            ingredientsOfRecipe1.add(addIngrToRec("Omelette a la Uwe", "Eggs", 2, 130));
            ingredientsOfRecipe1.add(addIngrToRec("Omelette a la Uwe", "Butter", 1, 10));
            ingredientsOfRecipe1.add(addIngrToRec("Omelette a la Uwe", "Olive Oil", 1, 100));
            ingredientsOfRecipe1.add(addIngrToRec("Omelette a la Uwe", "Parsley", 1, 10));

            List<RecipeIngredient> ingredientsOfRecipe2 = new ArrayList<>();
            ingredientsOfRecipe2.add(addIngrToRec("Mona's Creamy Baked Pears", "Pear", 2, 300));
            ingredientsOfRecipe2.add(addIngrToRec("Mona's Creamy Baked Pears", "Butter", 2, 20));
            ingredientsOfRecipe2.add(addIngrToRec("Mona's Creamy Baked Pears", "Sugar", 2, 20));
            ingredientsOfRecipe2.add(addIngrToRec("Mona's Creamy Baked Pears", "Whipped Cream", 1, 120));

            List<RecipeIngredient> ingredientsOfRecipe3 = new ArrayList<>();
            ingredientsOfRecipe3.add(addIngrToRec("The Hummus Of Jonas", "Garlic", 2, 10));
            ingredientsOfRecipe3.add(addIngrToRec("The Hummus Of Jonas", "Chickpeas", 50, 500));
            ingredientsOfRecipe3.add(addIngrToRec("The Hummus Of Jonas", "Olive Oil", 1, 100));

            List<RecipeIngredient> ingredientsOfRecipe4 = new ArrayList<>();
            ingredientsOfRecipe4.add(addIngrToRec("Fab'licious Spicy Lime Grilled Shrimp", "Shrimps", 3, 300));
            ingredientsOfRecipe4.add(addIngrToRec("Fab'licious Spicy Lime Grilled Shrimp", "Olive Oil", 1, 100));
            ingredientsOfRecipe4.add(addIngrToRec("Fab'licious Spicy Lime Grilled Shrimp", "Lime", 1, 100));
            ingredientsOfRecipe4.add(addIngrToRec("Fab'licious Spicy Lime Grilled Shrimp", "Vegetable Oil", 1, 10));

            List<RecipeIngredient> ingredientsOfRecipe5 = new ArrayList<>();
            ingredientsOfRecipe5.add(addIngrToRec("Jill Grill Baked Dijon Salmon", "Salmon", 4, 110));
            ingredientsOfRecipe5.add(addIngrToRec("Jill Grill Baked Dijon Salmon", "Mustard", 3, 30));
            ingredientsOfRecipe5.add(addIngrToRec("Jill Grill Baked Dijon Salmon", "Breadcrumbs", 4, 50));
            ingredientsOfRecipe5.add(addIngrToRec("Jill Grill Baked Dijon Salmon", "Butter", 2, 20));

            List<RecipeIngredient> ingredientsOfRecipe6 = new ArrayList<>();
            ingredientsOfRecipe6.add(addIngrToRec("Tasty Pumpkin Soup by Umut", "Pumpkin", 1, 900));
            ingredientsOfRecipe6.add(addIngrToRec("Tasty Pumpkin Soup by Umut", "Chicken Broth", 1, 900));
            ingredientsOfRecipe6.add(addIngrToRec("Tasty Pumpkin Soup by Umut", "Sugar", 1, 5));
            ingredientsOfRecipe6.add(addIngrToRec("Tasty Pumpkin Soup by Umut", "Greek Yoghurt", 1, 900));

            List<RecipeIngredient> ingredientsOfRecipe7 = new ArrayList<>();
            ingredientsOfRecipe7.add(addIngrToRec("Maja's Dreamy Chickpea Curry With Rice", "Basmati Rice", 2, 150));
            ingredientsOfRecipe7.add(addIngrToRec("Maja's Dreamy Chickpea Curry With Rice", "Chickpeas", 1, 900));
            ingredientsOfRecipe7.add(addIngrToRec("Maja's Dreamy Chickpea Curry With Rice", "Vegetable Oil", 1, 900));
            ingredientsOfRecipe7.add(addIngrToRec("Maja's Dreamy Chickpea Curry With Rice", "Pumpkin", 1, 900));

            List<RecipeIngredient> ingredientsOfRecipe8 = new ArrayList<>();
            ingredientsOfRecipe8.add(addIngrToRec("Crispy Tim Bites Bruschetta", "Eggplant", 1, 250));
            ingredientsOfRecipe8.add(addIngrToRec("Crispy Tim Bites Bruschetta", "Garlic", 2, 5));
            ingredientsOfRecipe8.add(addIngrToRec("Crispy Tim Bites Bruschetta", "Olive Oil", 1, 10));
            ingredientsOfRecipe8.add(addIngrToRec("Crispy Tim Bites Bruschetta", "Tomato", 4, 400));
            ingredientsOfRecipe8.add(addIngrToRec("Crispy Tim Bites Bruschetta", "Baguette", 1, 300));


            List<RecipeIngredient> ingredientsOfRecipe9 = new ArrayList<>();
            ingredientsOfRecipe9.add(addIngrToRec("Alex Loves Tasty Chicken Bake", "Chicken Breast", 3, 700));
            ingredientsOfRecipe9.add(addIngrToRec("Alex Loves Tasty Chicken Bake", "Basil Pesto", 1, 16));
            ingredientsOfRecipe9.add(addIngrToRec("Alex Loves Tasty Chicken Bake", "Tomato", 2, 250));
            ingredientsOfRecipe9.add(addIngrToRec("Alex Loves Tasty Chicken Bake", "Mozzarella", 2, 250));


            addRecIngrToDatabase(ingredientsOfRecipe1);
            addRecIngrToDatabase(ingredientsOfRecipe2);
            addRecIngrToDatabase(ingredientsOfRecipe3);
            addRecIngrToDatabase(ingredientsOfRecipe4);
            addRecIngrToDatabase(ingredientsOfRecipe5);
            addRecIngrToDatabase(ingredientsOfRecipe6);
            addRecIngrToDatabase(ingredientsOfRecipe7);
            addRecIngrToDatabase(ingredientsOfRecipe8);
            addRecIngrToDatabase(ingredientsOfRecipe9);

        }
    }
}
