package de.awacademy.tollesprojekt.backend.recipe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * RecipeService is a service of the Recipe table
 */
@Service
public class RecipeService {

    private RecipeRepository recipeRepository;

    @Autowired
    public RecipeService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    /**
     * The method is used for getting all Recipes from the database
     *
     * @return the return value is a list of Recipes
     */
    public List<Recipe> getAllRecipes() {
        return recipeRepository.findAll();
    }

    /**
     * The method is used for getting the Recipe details of a specific Recipe from the database
     *
     * @param id is the ID of a Recipe
     * @return the return value is a Recipe
     */
    public Recipe getRecipeById(long id) {
        return recipeRepository.findRecipeById(id);
    }

    /**
     * The method is used for adding a Recipe to the database
     *
     * @param recipe is a Recipe object
     */
    public void addRecipe(Recipe recipe) {
        recipe = new Recipe(recipe.getName(), recipe.getDescription(),
                recipe.getServings(), recipe.getRecipeIngredients(), recipe.getPicture(),
                recipe.getAlt(), recipe.getPrepCookTime(), recipe.isBreakfast(), recipe.isLunch(), recipe.isDinner(), recipe.isSnack());
        recipeRepository.save(recipe);
    }

    public RecipeRepository getRecipeRepository() {
        return recipeRepository;
    }

    /**
     * The method is used for getting a Recipe by its name from the database
     *
     * @param name is the name of the Recipe
     * @return the return value is a Recipe
     */
    public Recipe getRecipeByName(String name){
        return recipeRepository.findRecipeByName(name);
    }
}
