package de.awacademy.tollesprojekt.backend.manyToMany;

import de.awacademy.tollesprojekt.backend.recipe.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * RecipeIngredientService is a service of the RecipeIngredient table
 */
@Service
public class RecipeIngredientService {

    private RecipeIngredientRepository recipeIngredientRepository;
    private RecipeRepository recipeRepository;

    @Autowired
    public RecipeIngredientService(RecipeIngredientRepository recipeIngredientRepository,
                                   RecipeRepository recipeRepository) {
        this.recipeIngredientRepository = recipeIngredientRepository;
        this.recipeRepository = recipeRepository;
    }

    /**
     * The method is used for getting the RecipeIngredients by ID from the database
     *
     * @param id is the ID of the RecipeIngredient
     * @return the return value is a Set of RecipeIngredients
     */
    public Set<RecipeIngredient> getRecipeIngredientsByRecipeId(long id) {
        return recipeIngredientRepository.findAllByRecipeId(id);
    }

    /**
     * The method is used for adding a RecipeIngredient to the database
     *
     * @param recipeIngredient is the RecipeIngredient that will be saved in the database
     */
    public void addRecipeIngredient(RecipeIngredient recipeIngredient) {
        if (recipeIngredient != null) {
            recipeIngredientRepository.save(recipeIngredient);
            Recipe recipe = recipeIngredient.getRecipe();
            recipe.setRecipeIngredients(recipeIngredientRepository.findAllByRecipeId(recipe.getId()));
            recipe.setId(recipe.getId());
            recipeRepository.save(recipe);
        }
    }

    /**
     * The method is used for getting a RecipeIngredient by the Ingredient ID
     *
     * @param ingredientId is the ID of the Ingredient
     * @return the return value is a RecipeIngredient
     */
    public Set<RecipeIngredient> getRecipeIngredientsByIngredientId(long ingredientId) {
        return recipeIngredientRepository.findByIngredient_Id(ingredientId);
    }
}
