## Reverse Cookbook

Reverse Cookbook website that displays recipes and their details (name, preparation time, ingredients, preparation steps). The user can register and log in, in order to add and remove recipes from favourites in their profile page.

The user can use the search bar to find specific recipes, or use the checkboxes to filter the recipes based on the chosen ingredients.

Reverse Cookbook is a full-stack project, that uses Java, Spring, JPA, Hibernate and MariaDB on the Backend side, as well as TypeScript, Angular, HTML, CSS and Bootstrap on the Frontend side.